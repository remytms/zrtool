zrtool 0.2.0 (2020-11-14)
=========================

Features
--------

- Add new command `files` to list the content of a zrpa archive. (files)
- CLI now check that flac is properly installed and warn if not. (#1)
- New ZRPA library that ease managing ZRPA file. (#3)
- Add extract command that let you extract a specific file or a bunch of
  file from the archive. (#4)
- Add file command that let you add or update file into the archive. (#5)
- Add tags command that shows metadata in a ZRPA archive. (#6)
- New get command to extract an audio file with name pattern. (#7)


Improved Documentation
----------------------

- New version of file format ZRPA that is easier to deal with. (#2)


zrtool 0.1.0 (2020-10-29)
=========================

Features
--------

- Add archive command (archive)
- Add export command (export)
- The flac binary can be changed by using the ZRTOOL_FLAC_BIN
  environnement variable. (flacbin)
