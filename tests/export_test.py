# Copyright 2017-2019 Rémy Taymans <remytms@tsmail.eu>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Test for export subcommands"""

import filecmp
import shutil
from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_export():
    """
    Test that an archive is the same before archiving and after
    exporting.
    """
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    pjdircontent = [p.name for p in pjdir.iterdir() if p.is_file()]
    pjdircontent += ["AUDIO/" + p.name for p in (pjdir / "AUDIO").iterdir()]
    with runner.isolated_filesystem():
        # Exporting without metadata
        zrpafile = (Path() / "proj023.zrpa").resolve()
        arcres = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.json"),
                str(pjdir),
                str(zrpafile),
            ],
        )
        assert arcres.exit_code == 0
        expres = runner.invoke(
            cli.export_cmd, ["--number", "23", str(zrpafile)]
        )
        assert expres.exit_code == 0
        match, mismatch, errors = filecmp.cmpfiles(
            str(pjdir), "PROJ023", pjdircontent
        )
        assert len(mismatch) == 0
        assert len(errors) == 0
        assert len(match) == len(pjdircontent)
        zrpafile.unlink()
        shutil.rmtree("PROJ023")
        # Exporting with metadata
        arcres = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.json"),
                str(pjdir),
                str(zrpafile),
            ],
        )
        assert arcres.exit_code == 0
        expres = runner.invoke(
            cli.export_cmd, ["--with-tags", "--number", "23", str(zrpafile)]
        )
        assert expres.exit_code == 0
        match, mismatch, errors = filecmp.cmpfiles(
            str(pjdir), "PROJ023", pjdircontent + ["metadata.json"]
        )
        assert len(mismatch) == 0
        assert len(errors) == 1
        assert len(match) == len(pjdircontent)

        # Test guess number
        expres = runner.invoke(cli.export_cmd, [str(zrpafile)])
        assert expres.exit_code == 0
        assert Path("PROJ024").exists()

        # Test fail command
        expres = runner.invoke(
            cli.export_cmd, ["--number", "24", str(zrpafile)]
        )
        assert expres.exit_code != 0
