# Copyright 2019-2020 Rémy Taymans <remytms@tsmail.eu>
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for files subcommands"""

import shutil
from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_files():
    """Test files subcommand."""
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    assert pjdir.exists()
    with runner.isolated_filesystem():
        tmppjdir = Path("PROJ023")
        zrpafile = Path("proj023.zrpa")
        shutil.copytree(pjdir, tmppjdir)
        runner.invoke(
            cli.archive_cmd, ["--keep", str(tmppjdir), str(zrpafile)]
        )
        # Test with no patterns
        result = runner.invoke(cli.files_cmd, [str(zrpafile)])
        files = [
            "AUDIO/",
            "AUDIO/MASTR000.WAV",
            "AUDIO/MASTR001.WAV",
            "AUDIO/MONO-000.WAV",
            "AUDIO/MONO-001.WAV",
            "AUDIO/MONO-002.WAV",
            "AUDIO/MONO-003.WAV",
            "AUDIO/MONO-004.WAV",
            "AUDIO/MONO-005.WAV",
            "AUDIO/MONO-006.WAV",
            "AUDIO/MONO-007.WAV",
            "AUDIO/otherfile.txt",
            "EFXDATA.ZDT",
            "PRJDATA.ZDT",
            "metadata.json",
        ]
        assert result.output.strip().split("\n") == files
        # Test with patterns
        result = runner.invoke(
            cli.files_cmd, [str(zrpafile), "*.txt", "*.ZDT"]
        )
        files = [
            "AUDIO/otherfile.txt",
            "EFXDATA.ZDT",
            "PRJDATA.ZDT",
        ]
        assert result.output.strip().split("\n") == files
