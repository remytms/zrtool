# Copyright 2020 Rémy Taymans <remytms@tsmail.eu>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Test for extract subcommands"""

from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_file():
    """
    Test updating the archive.
    """
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    with runner.isolated_filesystem():
        zrpafile = (Path() / "proj023.zrpa").resolve()
        arcres = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.json"),
                str(pjdir),
                str(zrpafile),
            ],
        )
        assert arcres.exit_code == 0

        # Test adding a WAV file
        audio = testdir / "data/audiofile.wav"
        fileres = runner.invoke(cli.file_cmd, [str(zrpafile), str(audio)])
        assert fileres.exit_code == 0
        files = [
            "AUDIO/",
            "AUDIO/MASTR000.WAV",
            "AUDIO/MASTR001.WAV",
            "AUDIO/MONO-000.WAV",
            "AUDIO/MONO-001.WAV",
            "AUDIO/MONO-002.WAV",
            "AUDIO/MONO-003.WAV",
            "AUDIO/MONO-004.WAV",
            "AUDIO/MONO-005.WAV",
            "AUDIO/MONO-006.WAV",
            "AUDIO/MONO-007.WAV",
            "AUDIO/audiofile.wav",
            "AUDIO/otherfile.txt",
            "EFXDATA.ZDT",
            "PRJDATA.ZDT",
            "metadata.json",
        ]
        filesre = runner.invoke(cli.files_cmd, [str(zrpafile)])
        assert filesre.output.strip().split("\n") == files

        # Test updating a file
        audio = testdir / "data/audiofile.wav"
        fileres = runner.invoke(cli.file_cmd, [str(zrpafile), str(audio)])
        assert fileres.exit_code != 0
        fileres = runner.invoke(
            cli.file_cmd, ["--update", str(zrpafile), str(audio)]
        )
        assert fileres.exit_code == 0
        files = [
            "AUDIO/",
            "AUDIO/MASTR000.WAV",
            "AUDIO/MASTR001.WAV",
            "AUDIO/MONO-000.WAV",
            "AUDIO/MONO-001.WAV",
            "AUDIO/MONO-002.WAV",
            "AUDIO/MONO-003.WAV",
            "AUDIO/MONO-004.WAV",
            "AUDIO/MONO-005.WAV",
            "AUDIO/MONO-006.WAV",
            "AUDIO/MONO-007.WAV",
            "AUDIO/audiofile.wav",
            "AUDIO/otherfile.txt",
            "EFXDATA.ZDT",
            "PRJDATA.ZDT",
            "metadata.json",
        ]
        filesre = runner.invoke(cli.files_cmd, [str(zrpafile)])
        assert filesre.output.strip().split("\n") == files

        # Test adding a normal file
        normalfile = testdir / "data/metadata.wrong"
        fileres = runner.invoke(cli.file_cmd, [str(zrpafile), str(normalfile)])
        assert fileres.exit_code == 0
        files = [
            "AUDIO/",
            "AUDIO/MASTR000.WAV",
            "AUDIO/MASTR001.WAV",
            "AUDIO/MONO-000.WAV",
            "AUDIO/MONO-001.WAV",
            "AUDIO/MONO-002.WAV",
            "AUDIO/MONO-003.WAV",
            "AUDIO/MONO-004.WAV",
            "AUDIO/MONO-005.WAV",
            "AUDIO/MONO-006.WAV",
            "AUDIO/MONO-007.WAV",
            "AUDIO/audiofile.wav",
            "AUDIO/otherfile.txt",
            "EFXDATA.ZDT",
            "PRJDATA.ZDT",
            "metadata.json",
            "metadata.wrong",
        ]
        filesre = runner.invoke(cli.files_cmd, [str(zrpafile)])
        assert filesre.output.strip().split("\n") == files

        # Test adding with a destination dir
        normalfile = testdir / "data/metadata.wrong"
        fileres = runner.invoke(
            cli.file_cmd, ["--dest", "data", str(zrpafile), str(normalfile)]
        )
        assert fileres.exit_code == 0
        files = [
            "AUDIO/",
            "AUDIO/MASTR000.WAV",
            "AUDIO/MASTR001.WAV",
            "AUDIO/MONO-000.WAV",
            "AUDIO/MONO-001.WAV",
            "AUDIO/MONO-002.WAV",
            "AUDIO/MONO-003.WAV",
            "AUDIO/MONO-004.WAV",
            "AUDIO/MONO-005.WAV",
            "AUDIO/MONO-006.WAV",
            "AUDIO/MONO-007.WAV",
            "AUDIO/audiofile.wav",
            "AUDIO/otherfile.txt",
            "EFXDATA.ZDT",
            "PRJDATA.ZDT",
            "data/metadata.wrong",
            "metadata.json",
            "metadata.wrong",
        ]
        filesre = runner.invoke(cli.files_cmd, [str(zrpafile)])
        assert filesre.output.strip().split("\n") == files
