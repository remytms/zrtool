# Copyright 2020 Rémy Taymans <remytms@tsmail.eu>
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for get subcommands"""

import shutil
from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_get():
    """Test get subcommand."""
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    metadatapath = testdir / "data/metadata.json"
    assert pjdir.exists()
    with runner.isolated_filesystem():
        tmppjdir = Path("PROJ023")
        zrpafile = Path("proj023.zrpa")
        shutil.copytree(pjdir, tmppjdir)
        runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(metadatapath),
                str(tmppjdir),
                str(zrpafile),
            ],
        )
        # Test fail command
        result = runner.invoke(cli.get_cmd, [str(zrpafile)])
        assert result.exit_code != 0
        result = runner.invoke(cli.get_cmd, [str(zrpafile), "metadata.json"])
        assert result.exit_code != 0
        result = runner.invoke(
            cli.get_cmd, [str(zrpafile), "MASTR000.WAV", "metadata.json"]
        )
        assert result.exit_code != 0
        # Get with _filename
        result = runner.invoke(
            cli.get_cmd, [str(zrpafile), "--format", "wav", "MASTR000.WAV"]
        )
        mastr0name = runner.invoke(
            cli.tags_cmd,
            [str(zrpafile), "--key", "_filename", "--file", "MASTR000.WAV"],
        )
        assert Path(mastr0name.output.strip()).exists()
        # Get with --name
        result = runner.invoke(
            cli.get_cmd,
            [
                str(zrpafile),
                "--format",
                "flac",
                "--name",
                "$author - $instrument.flac",
                "MONO-000.WAV",
            ],
        )
        assert result.exit_code == 0
        mono0author = runner.invoke(
            cli.tags_cmd,
            [str(zrpafile), "--key", "author", "--file", "MONO-000.WAV"],
        )
        mono0instrument = runner.invoke(
            cli.tags_cmd,
            [str(zrpafile), "--key", "instrument", "--file", "MONO-000.WAV"],
        )
        assert Path(
            "{author} - {instru}.flac".format(
                author=mono0author.output.strip(),
                instru=mono0instrument.output.strip(),
            )
        ).exists()
