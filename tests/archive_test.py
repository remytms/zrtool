# Copyright 2019-2020 Rémy Taymans <remytms@tsmail.eu>
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for archive subcommands"""

import shutil
from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_archive():
    """Test archive subcommand."""
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    assert pjdir.exists()
    # Test that archive command fails with no args
    result = runner.invoke(cli.archive_cmd)
    assert result.exit_code == 2
    with runner.isolated_filesystem():
        tmppjdir = Path("PROJ023")
        zrpafile = Path("proj023.zrpa")
        # Test with no --keep argument
        assert pjdir.exists()
        shutil.copytree(pjdir, tmppjdir)
        assert tmppjdir.exists()
        assert not zrpafile.exists()
        result = runner.invoke(cli.archive_cmd, [str(tmppjdir), str(zrpafile)])
        assert not result.output
        assert result.exit_code == 0
        assert not tmppjdir.exists()
        assert zrpafile.exists()
        zrpafile.unlink()
        # Test with --keep argument
        shutil.copytree(pjdir, tmppjdir)
        result = runner.invoke(
            cli.archive_cmd, ["--keep", str(tmppjdir), str(zrpafile)]
        )
        assert result.exit_code == 0
        assert tmppjdir.exists()
        # Test overwrite output file
        result = runner.invoke(
            cli.archive_cmd,
            ["--keep", str(tmppjdir), str(zrpafile)],
        )
        assert result.exit_code == 1
        result = runner.invoke(
            cli.archive_cmd,
            ["--keep", str(tmppjdir), str(zrpafile)],
            input="y\n",
        )
        assert result.exit_code == 0
        assert zrpafile.exists()
        zrpafile.unlink()
        # Test metadata json
        result = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.json"),
                str(tmppjdir),
                str(zrpafile),
            ],
        )
        assert not result.output
        assert result.exit_code == 0
        assert zrpafile.exists()
        zrpafile.unlink()
        # Test metadata yaml
        result = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.yml"),
                str(tmppjdir),
                str(zrpafile),
            ],
        )
        assert result.exit_code == 0
        assert zrpafile.exists()
        zrpafile.unlink()
        # Test wrong metadata file type
        result = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.wrong"),
                str(tmppjdir),
                str(zrpafile),
            ],
        )
        assert result.exception
        assert not zrpafile.exists()
