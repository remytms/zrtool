# Copyright 2020 Rémy Taymans <remytms@tsmail.eu>
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for tags subcommands"""

import shutil
from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_tags():
    """Test tags subcommand."""
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    metadatapath = testdir / "data/metadata.json"
    assert pjdir.exists()
    with runner.isolated_filesystem():
        tmppjdir = Path("PROJ023")
        zrpafile = Path("proj023.zrpa")
        shutil.copytree(pjdir, tmppjdir)
        runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(metadatapath),
                str(tmppjdir),
                str(zrpafile),
            ],
        )
        # Test fail command
        # key used alone
        result = runner.invoke(cli.tags_cmd, ["--key", "title", str(zrpafile)])
        assert result.exit_code != 0
        # key used with multiple --file
        result = runner.invoke(
            cli.tags_cmd,
            [
                "--file",
                "MASTR000.WAV",
                "--file",
                "MONO-000.WAV",
                "--key",
                "title",
                str(zrpafile),
            ],
        )
        assert result.exit_code != 0
        # key does not exists
        result = runner.invoke(
            cli.tags_cmd,
            ["--file", "MASTR000.WAV", "--key", "title", str(zrpafile)],
        )
        assert result.exit_code != 0
        # file does not exists
        result = runner.invoke(
            cli.tags_cmd,
            ["--file", "WRONGFILE.WAV", "--key", "title", str(zrpafile)],
        )
        assert result.exit_code != 0
        result = runner.invoke(
            cli.tags_cmd, ["--file", "WRONGFILE.WAV", str(zrpafile)]
        )
        assert result.exit_code != 0

        # Test --key
        result = runner.invoke(
            cli.tags_cmd, ["--project", "--key", "title", str(zrpafile)]
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Song title"
        result = runner.invoke(
            cli.tags_cmd,
            ["--file", "MONO-000.WAV", "--key", "author", str(zrpafile)],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Someone"

        # Test showing all metadata
        result = runner.invoke(cli.tags_cmd, [str(zrpafile)])
        assert result.exit_code == 0
        assert "title: Song title" in result.output
        assert "  MONO-000.WAV:" in result.output
        assert "    author: Someone" in result.output

        # Test showing file metadata
        result = runner.invoke(
            cli.tags_cmd, ["--file", "MONO-000.WAV", str(zrpafile)]
        )
        assert result.exit_code == 0
        assert "MONO-000.WAV:" in result.output
        assert "  author: Someone" in result.output
