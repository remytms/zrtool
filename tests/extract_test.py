# Copyright 2020 Rémy Taymans <remytms@tsmail.eu>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Test for extract subcommands"""

import filecmp
from pathlib import Path

from click.testing import CliRunner

from zrtool import cli


def test_extract():
    """
    Test that files can be extracted form an archive.
    """
    runner = CliRunner()
    testdir = Path(__file__).parent.resolve()
    pjdir = testdir / "data/PROJ023"
    with runner.isolated_filesystem():
        zrpafile = (Path() / "proj023.zrpa").resolve()
        arcres = runner.invoke(
            cli.archive_cmd,
            [
                "--keep",
                "--metadata",
                str(testdir / "data/metadata.json"),
                str(pjdir),
                str(zrpafile),
            ],
        )
        assert arcres.exit_code == 0
        # Test WAV file
        extres = runner.invoke(cli.extract_cmd, [str(zrpafile), "*MASTR000*"])
        assert extres.exit_code == 0
        extractedfile = Path("AUDIO/MASTR000.WAV")
        assert filecmp.cmp(
            str(pjdir / "AUDIO/MASTR000.WAV"), str(extractedfile)
        )
        extractedfile.unlink()
        # Test other files
        extres = runner.invoke(cli.extract_cmd, [str(zrpafile), "PRJ*"])
        assert extres.exit_code == 0
        extractedfile = Path("PRJDATA.ZDT")
        assert filecmp.cmp(str(pjdir / "PRJDATA.ZDT"), str(extractedfile))

        # Test fail command
        extres = runner.invoke(
            cli.extract_cmd, ["--directory", "wrongdirectory", str(zrpafile)]
        )
        assert extres.exit_code != 0
