# Copyright 2019-2020 Rémy Taymans <remytms@tsmail.eu>
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for archive subcommands"""

from click.testing import CliRunner

import zrtool
from zrtool import cli


def test_main():
    """Test main function"""
    zrtool.FLACBIN = "awrongflacbin"
    runner = CliRunner(mix_stderr=False)
    result = runner.invoke(cli.main, ["export", "--help"])
    assert "Warning" in result.stderr
    assert "Usage" in result.stdout
