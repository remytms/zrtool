File format for Zoom R project specification - Version 2.0
==========================================================

Zoom R devices are multitrack audio recorder. Zoom R devices saves audio
recording session in a proprietary format.

This document propose an open source file format to handle metadata
associated to the recording session and improve disk space usage.



Original file format
--------------------

This part will describe the original file format used by the recorder.

A recording session is saved like this by the recorder.

```
PROJ006
├── AUDIO
│   ├── MASTR000.WAV
│   ├── MONO-000.WAV
│   ├── MONO-001.WAV
│   ├── MONO-002.WAV
│   ├── MONO-003.WAV
│   ├── MONO-004.WAV
│   └── MONO-005.WAV
├── EFXDATA.ZDT
└── PRJDATA.ZDT
```

A recording session is named a "project" by the device. By default
project are named "PROJ" + 3 digits that are incremented each time a
new project is created. The counter begins at zero when the storage
medium is empty. The name of the root directory must conform to the rule
"PROJ" + 3 digits in order to be recognised by the recorder. Therefor
this name can vary depending on the storage medium that will be used on
the recorder. For example, the root of the same project can be named
"PROJ006" on a given storage medium and can be named "PROJ010" on
another medium because a "PROJ006" already exist on it.

The name "PROJ006" is also saved as the name of the project in the
`PRJDATA.ZDT` file. But when the name of the project is modified in the
`PRJDATA.ZDT` file, the name of the directory is not changed
accordingly. So there is no link between the name of the project and the
name of the root directory, except that by default there are the same.
Also the name of the root directory is used by the recorder to give them
a sequence, and order them properly. The name of the project is useful
to differentiate one from another.

The directory `AUDIO` contains wave file that can be played or recorded
by the recorder. These files are referenced in the `PRJDATA.ZDT` file.
Their names can not be longer than 8 ASCII characters (excluding the
".WAV" extension).

At the root of the project directory there is also the `PRJDATA.ZDT`
file that contains configurations of the project in a binary and
proprietary format that can be read by the `zoomrlib` python library.
The `EFXDATA.ZDT` contains information about configurations of the
effects provided by the device.

As long as special file are properly named and present in the root
directory, it will be considered as valid data format and can be read
from the recorder. As a consequence, any file can be added in the root
directory. These files can contain information that can not be stored
elsewhere in the project.



What needs to be improved
-------------------------

This part will describe what's missing in this file format and what can
be improved.


### Metadata

In the original format, the only metadata that is stored is the name of
the project and it should fit in 8 ASCII characters. That's totally not
enough.

This can be solved by adding a JSON file that can store metadata about
the project.

Metadata can also be attached to a specific file, like information about
the way it was recorded, the person who performed, etc. These
information can also go in a JSON file.


### Disk space

In the original format, most of the files are WAVE files, but there is
also binary files. These are not disk space efficient.

To gain space, file can be compressed. LZMA works very well for binary
files. However, for WAVE file, LZMA is not optimal but in the other hand
it's completely lossless so that the exact same original file can be
retrieved. Another method for WAVE file is to convert it to FLAC file,
this is, by far, the most efficient method in term of disk space usage.
But it's bad part is that the original WAVE file that can be retrieved
by converting back the FLAC file to a WAVE file is not bit to bit the
same as the original one.



Specification of the open file format (ZRPA)
--------------------------------------------

The new file format is a simple tar file with extension `.zrpa` (zoomr
project archive).

```
project_name.zrpa
├── AUDIO/
│   ├── MASTR000.WAV.{flac,xz}
│   ├── MONO-000.WAV.{flac,xz}
│   ├── MONO-001.WAV.{flac,xz}
│   ├── MONO-002.WAV.{flac,xz}
│   ├── MONO-003.WAV.{flac,xz}
│   ├── MONO-004.WAV.{flac,xz}
│   ├── MONO-005.WAV.{flac,xz}
│   └── ...
├── EFXDATA.ZDT.xz
├── metadata.json.xz
├── PRJDATA.ZDT.xz
└── ...
```

The main tar archive contains two elements:

- The original files from the default format compressed.
- The file `metadata.json`, a new metadata file in JSON containing the
  metadata of the project.

This file format is based on the tar format. Therefor, a file can be
found in the archive several times. The last one is the right version of
the file.

Owner and group should be reset when adding a file to a ZRPA archive,
but date should be preserved.


### Audio files

Files found in the original format should be WAVE file. There is
two choice for compressing these WAVE file: LZMA or FLAC.

If FLAC is chosen, all WAVE file will be converted to FLAC. The
extension `flac` is added to the original file name (the original
extension is preserved).

If the LZMA compression is chosen, all WAVE file will be compressed
using LZMA.  The extension `xz` is added to the original file name (the
original extension is preserved).

Preserving the original extension ease the way of retrieving the
original file name, because this file name are also stored in the
`PRJDATA.ZDT` file.

All audio files found in the original project will be compressed with
the previous rules, even if they are not in the `AUDIO` directory.

### Other files

Other files found in the original format will be lzma-compressed and
added to the archive.


### metadata.json

The `metadata.json` file is contains two parts. The first is named
`project` and the second is named `audio`.

The project part contains a set of keys and values that can be freely
choose. These are the metadata of the project.

The audio part contains a set with keys matching the file name (without
the extension) found in the `audio.tar` archive. For each file there is
a set of keys and values that represent metadata for the specified file.
These metadata can be freely choose.

Keys for metadata should conform to theses rules:

- keys begining with two underscores (`__`) can not be created by user.
  It's reserved for system use.
- keys begining with one underscore (`_`) are special keys that are
  described by the specification and that can be edited by a user.
- keys are case insensitive, keys are always stored lowercase.

There is a special keys that can be define, which is `_filename`. This
key can be used to give a longer name to the file that is attached to.
This name will be used when extract the file from the archive. But will
not be used when converting the new format to the original one which is
compatible with the zoom device.

Here is a example of a metadata file:

```json
{
    "project": {
        "title": "Song title",
        "author": "Band name",
        "date": "2020-02-29",
        "take": 0
    },
    "audio": {
        "MONO-000.WAV": {
            "author": "Someone",
            "instrument": "guitar",
            "comment": "Recorded with simple microphone"
        },
        "MASTR000.WAV": {
            "_filename": "session.2020-02-29.with-guitar.wav",
            "comment": "First mix done. Needs to be improved"
        }
    }
}
```
